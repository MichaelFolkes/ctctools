



#'(data base) Create a connection to an MS Access database using ODBC
#'
#' @param db_path Path and filename to an MS Access database
#'
#' @return A ODBC connection to an MS Access databases
#'
#' @importFrom odbc odbc
#'
#' @export
#'
connectAccessDb <- function(db_path) {
  driverName <- "Driver={Microsoft Access Driver (*.mdb, *.accdb)};"
  driverName <- paste0(driverName, "DBQ=", normalizePath(db_path))

  cn <- odbc::dbConnect(odbc::odbc(), .connection_string = driverName)
}#END connectAccessDb





#' (data base) Retreives data based on an Sql file, binds parameters to the text
#'
#' @param db_conn Datababase connection
#' @param sql_file_name The name of of the file with sql to load
#' @param params List of named parameters to bind to the sql text
#' @param sql_dir The directory that stores all the sql statements (defaults to "sql")
#' @param clean_sql A boolean that identifies if the format of the final sql should be cleaned (e.g. strip newlines)
#' @param package_name The package name to load sql file from, defaults to the current package name
#'
#' @return Results from the query
#'
#' @importFrom utils packageName
#' @importFrom glue glue
#' @importFrom DBI dbGetQuery
#'
#'
#' @export
#'
getSqlScriptResult <- function(db_conn, file_name, params = NULL, dir = "sql", clean_sql = TRUE, package_name = packageName(parent.frame())) {
  text_file_path <- system.file(dir, file_name, package = package_name)
  if (file.exists(text_file_path) == FALSE) {
    cat(glue("ERROR - The file '{paste0(dir, file_name)}' is missing from the {package_name} package."))
    stop()
  }
  text <- readChar(text_file_path, file.info(text_file_path)$size)

  if (clean_sql) {
     text <- gsub("\r\n", " ", text)
  }

  if (!is.null(params)) {
    text <- glue(text, .envir = params)
  }

  data_df <- dbGetQuery(db_conn, text)
  return(data_df)
}#END getSqlScriptResult




#' Query any data base
#'
#' @param path A character vector, length one. File path to the data base.
#' @param sql A character vector, length one. The SQL statement.
#'
#' @return A data frame based on the SQL
#' @export
#'
#' @importFrom odbc dbConnect dbGetQuery dbDisconnect
#'
#' @examples
#' \dontrun{
#'
#' filepath <- "C:/CASClient_2020_BE.mdb"
#' sql <- "SELECT CFileFishery.* FROM CFileFishery;"
#' df <- querydbase(path=filepath, sql=sql)
#' str(df)
#' }
querydbase <- function(path=NA, sql=NA){

  # driverName <- "Driver={Microsoft Access Driver (*.mdb, *.accdb)};"
  # driverName <- paste0(driverName, "DBQ=", path)
  #
  # cn <- odbc::dbConnect(odbc::odbc(), .connection_string=driverName)
  cn <- connectAccessDb(path)
  results <- odbc::dbGetQuery(cn, sql )

  odbc::dbDisconnect(cn)

  return(results)

}#END querydbase





#' @title (data base) Use the windows GUI to select a file
#'
#' @param display_caption What should the title of the file selection dialog say?
#'
#' @return A path and filename
#'
#' @export
selectAccessDb <- function(display_caption = "Select MS Access Database") {
  msaccess_filter <- rbind(Filters["All",], cbind("MS Access database (*.mdb)", "*.mdb"))


  db_filename <- choose.files(default = "", caption = display_caption,
                              multi = FALSE, filters = msaccess_filter,
                              index = nrow(Filters))

  return(db_filename)
}#END selectAccessDb


