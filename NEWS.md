# ctctools 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
* Added matchLocations function to compare tag code/location code combinations against fishery mapping

